from django.db import models

# Create your models here.

# Title: Un título con 200 caracteres de longitud máxima.

# Subtitle: Un subtítulo con 200 caracteres de longitud máxima.

# Content: Un texto de tamaño indefinido.

# Image: Una imagen para mostrar de fondo almacenada en el directorio services (dentro de media).

# Created: Un campo automático para gestionar la fecha y hora de creación.

# Updated: Un campo automático para gestionar la fecha y hora de última actualización.


class Service(models.Model):
    title = models.CharField(max_length=200, verbose_name='Título')
    subtitle = models.CharField(max_length=200, verbose_name='Subtítulo')
    content = models.TextField(verbose_name='Contenido')
    image = models.ImageField(verbose_name='Imagen', upload_to='services')
    created = models.DateTimeField(
        auto_now_add=True, verbose_name='Fecha de creación')
    updated = models.DateTimeField(
        auto_now=True, verbose_name='Fecha de actualización')

    class Meta:
        verbose_name = 'servicio'
        verbose_name_plural = 'servicios'
        ordering = ["-created"]

    def __str__(self):
        return self.title
